import React, { Component } from 'react'
import FunctionDemo from './FunctionDemo'
import {Button } from 'react-bootstrap'

export default class ClassDemo extends Component {

    constructor() {
        super()
        this.state =  {
            foods: [
                {id: 1, name: "pizza", qty:0}, 
                {id: 2, name: "french fries", qty:0},
                {id: 3, name: "chicken wings", qty:0}
            ]
        }            
    }

    addFoodItem = () => {
        // copy from state
        const myFoods = [...this.state.foods]

        // copy to new array,
            // don't copy what you don't want


        // (choose 1) using filter
        const temp = myFoods.filter(item => item.id !== 2)


        // (choose 1) using normal for loop
        const temp2 = [];
        var i = 0
        var item = {}

        for (i = 0; i < 3; i++){

        item = myFoods[i]

            if (item.id !== 2){
                temp2.push(item)
            }
        }

        

        // copy back to state
        this.setState({
            foods: temp
        })
    }

    render() {
        return (
            <div>
                {console.log(this.state.foods)}
                <FunctionDemo data={this.state.foods} />     
                <Button onClick={this.addFoodItem}>a</Button>           
            </div>            
        )
    }
}
