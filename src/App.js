import './App.css';
import ClassDemo from './components/ClassDemo'

function App() {
  return (
    <div>
      <ClassDemo />
    </div>
  );
}

export default App;
